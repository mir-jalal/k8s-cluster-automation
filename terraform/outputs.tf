output "master_addresses" {
  value = join(",", "${aws_instance.master_vm.*.public_ip}")
}

output "worker_addresses" {
  value = join(",", "${aws_instance.worker_vm.*.public_ip}")
}

output "load_balancer_ip"{
  value = "${aws_lb.master_lb.dns_name}"
}
