data "aws_ami" "cluster_ami" {
  most_recent = true

  filter {
    name 	= "name"
    values 	= [var.ami_name]
  }

  filter {
    name	= "virtualization-type"
    values 	= ["hvm"]
  }

  owners = [var.ami_owner]
}

resource "aws_instance" "master_vm" {
  count         = var.master_count
  ami		    = data.aws_ami.cluster_ami.id
  instance_type	= var.aws_instance_type
  key_name      = "ssh-key"

  subnet_id       = aws_subnet.cluster_public_subnet.id
  security_groups = [aws_security_group.cluster_security_group.id]

  tags = {
    Name = "Master"
  }
}

resource "aws_instance" "worker_vm" {
  count         = var.worker_count
  ami		    = data.aws_ami.cluster_ami.id
  instance_type	= var.aws_instance_type
  key_name      = "ssh-key"

  subnet_id       = aws_subnet.cluster_public_subnet.id
  security_groups = [aws_security_group.cluster_security_group.id]

  tags = {
    Name = "Worker"
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name    = "ssh-key"
  public_key  = var.devops_ssh_key
}

