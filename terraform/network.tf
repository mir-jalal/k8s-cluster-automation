resource "aws_vpc" "cluster_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "Cluster"
  }
}

resource "aws_subnet" "cluster_public_subnet" {
  cidr_block = "10.0.10.0/24"
  vpc_id = aws_vpc.cluster_vpc.id
  availability_zone = var.az_first
  map_public_ip_on_launch = true

  tags = {
    Name = "cluster_public_subnet"
  }
}

resource "aws_subnet" "cluster_private_subnet"{
  cidr_block = "10.0.20.0/24"
  vpc_id = aws_vpc.cluster_vpc.id
  availability_zone = var.az_first

  tags = {
    Name = "cluster_private_subnet"
  }
}

resource "aws_subnet" "cluster_private_subnet_reserved"{
  cidr_block = "10.0.30.0/24"
  vpc_id = aws_vpc.cluster_vpc.id
  availability_zone = var.az_second

  tags = {
    Name = "cluster_private_subnet"
  }
}

resource "aws_db_subnet_group" "cluster_db_subnet_group" {
  name = "cluster_db_subnet_group"
  subnet_ids = [aws_subnet.cluster_private_subnet.id, aws_subnet.cluster_private_subnet_reserved.id]

  tags = {
    Name = "cluster_db_subnet_group"
  }
}

resource "aws_security_group" "cluster_security_group" {
  vpc_id = aws_vpc.cluster_vpc.id

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 6443
    to_port          = 6443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH for VPC"
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.cluster_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "cluster_db_security_group" {
  vpc_id = aws_vpc.cluster_vpc.id

  ingress {
    description = "SSH for VPC"
    from_port = 3306
    protocol = "tcp"
    to_port = 3306
    cidr_blocks = [aws_vpc.cluster_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_internet_gateway" "cluster_internet_gateway" {
  vpc_id = aws_vpc.cluster_vpc.id
}

resource "aws_route_table" "cluster_route_table" {
  vpc_id = aws_vpc.cluster_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cluster_internet_gateway.id
  }
}

resource "aws_route_table_association" "cluster_route_table_association" {
  route_table_id = aws_route_table.cluster_route_table.id
  subnet_id = aws_subnet.cluster_public_subnet.id
}

resource "aws_lb" "master_lb" {
  name               = "master-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.cluster_public_subnet.id]

  enable_deletion_protection = false

  tags = {
    Name = "MasterLB"
  }
}

resource "aws_lb_listener" "master_lb_listener" {
  load_balancer_arn = aws_lb.master_lb.arn
  port              = "6443"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.master_lb_tg.arn
  }
}

resource "aws_lb_target_group" "master_lb_tg" {
  name     = "master-lb-tg"
  port     = 6443
  protocol = "TCP"
  vpc_id   = aws_vpc.cluster_vpc.id
  health_check {
    enabled = true
    port = 80
    protocol = "TCP"
  }
}

resource "aws_lb_target_group_attachment" "master_lb_tg_ga" {
  count = var.master_count
  target_group_arn = aws_lb_target_group.master_lb_tg.arn
  target_id        = aws_instance.master_vm[count.index].id
  port             = 6443
  
}

resource "aws_lb" "worker_lb" {
  name               = "worker-lb"
  internal           = false
  load_balancer_type = "network"
  subnets            = [aws_subnet.cluster_public_subnet.id]

  enable_deletion_protection = false

  tags = {
    Name = "WorkerLB"
  }
}

resource "aws_lb_listener" "worker_lb_listener_https" {
  load_balancer_arn = aws_lb.worker_lb.arn
  port              = "443"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.worker_lb_tg.arn
  }
}

resource "aws_lb_listener" "worker_lb_listener_http" {
  load_balancer_arn = aws_lb.worker_lb.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.worker_lb_tg.arn
  }
}

resource "aws_lb_target_group" "worker_lb_tg" {
  name     = "worker-lb-tg"
  port     = 443
  protocol = "TCP"
  vpc_id   = aws_vpc.cluster_vpc.id
  health_check {
    enabled = true
    port = 80
    protocol = "TCP"
  }
}

resource "aws_lb_target_group_attachment" "worker_lb_tg_ga" {
  count = var.worker_count
  target_group_arn = aws_lb_target_group.worker_lb_tg.arn
  target_id        = aws_instance.worker_vm[count.index].id
  port             = 443
}
