#!/usr/bin/bash

source apply.env
echo "[masters]" > hosts
count=0

for master in $(echo $MASTER_ADDRESS | tr "," "\n")
do  
  echo master${count} ansible_host=$master ansible_user=ubuntu ansible_ssh_extra_args=\'-o StrictHostKeyChecking=no\' ansible_ssh_private_key_file=${PRIVATE_KEY_FILE} >> hosts 
  ((count++)) 
done 
count=0
for worker in $(echo $WORKER_ADDRESS | tr "," "\n"); do 
  echo worker${count} ansible_host=$worker ansible_user=ubuntu ansible_ssh_extra_args=\'-o StrictHostKeyChecking=no\' ansible_ssh_private_key_file=${PRIVATE_KEY_FILE} >> hosts 
  ((count++)) 
done 
echo [all:vars] >> hosts
echo ansible_python_interpreter=/usr/bin/python3.8 >> hosts
